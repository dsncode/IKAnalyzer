/**
 * 
 */
package com.dsncode.tokenizer;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.dsncode.wltea.service.TokenizerService;

/**
 * @author daniel
 *
 */
public class TokenizerTest {
	
	@Test
	public void tokeinizer_should_extract_at_least_these_chinese_tokens()
	{
		List<String> rightTokens = Arrays.asList("臺灣大學","電子","郵");
		List<String> actualTokens = TokenizerService.tokenize("臺灣大學電子郵");
		//at least should contain the right tokens
		
		rightTokens.stream().forEach(token->{
			 assertThat(actualTokens, hasItems(token));
		});
		
		
	}
	

}
