package com.dsncode.wltea.service;

import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.wltea.analyzer.lucene.IKAnalyzer;

public class TokenizerService {

	private static Analyzer analyzer;
	static{
		 analyzer = new IKAnalyzer(true);	
	}
	
	public static List<String> tokenize(String text)
	{
		List<String> list = new LinkedList<String>();
	    TokenStream ts = null;
		try {
			ts = analyzer.tokenStream("myfield", new StringReader(text));
		    CharTermAttribute term = ts.addAttribute(CharTermAttribute.class);
			ts.reset(); 
			
			while (ts.incrementToken()) {
				list.add(term.toString());
			}
			ts.end();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(ts != null){
		      try {
				ts.close();
		      } catch (IOException e) {
				e.printStackTrace();
		      }
			}
	    }
		return list;
	}
		
	
}
