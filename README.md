# README #

in a nutshell. IKAnalyzer is a dictionary based analyzer for traditional/simplified chinese. this analyzer is compatible with solr 6.6.0.
this project is based on this original project https://code.google.com/archive/p/ik-analyzer/.

this software can be distributed following apache-licence 2.0
https://www.apache.org/licenses/LICENSE-2.0

### What is this repository for? ###

* IKAnalyzer is a solr traditional/simplified chinese tokenizer.
* 7.1. compatible with solr 7.1.0

### How do I get set up? ###

* how to run it: Compile "mvn package" and place jar into solr lib
* Dependencies: solr-core 7.1.0

### Who do I talk to if there are any issues or bugs? ###
* send me a msg

### may I contribute? ###
* sure! please send your sugerences, requests and pull request anytime :)
